#ifndef _TEST_H_
#define _TEST_H_

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>


void heap_initialize(size_t size);
void mem_aloc_test();
void release_one_block_test();
void release_two_blocks_test();
void memory_expansion_test();
void new_region_test();

#endif
