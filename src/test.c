#include "test.h"

void* heap = NULL;
void heap_initialize(size_t size) {
    heap = heap_init(size);
    debug_heap(stdout, heap);
    fprintf(stdout, "Куча инициализирована\n\n");
}

void mem_aloc_test() {
    fprintf(stdout, "\nТест один начался:\n\n");
    void* temp = _malloc(1000);
    if (!temp) {fprintf(stderr, "Произошла ошибка\n\n");}
    debug_heap(stdout, heap);
    _free(temp);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nТест один завершился\n\n");
}

void release_one_block_test() {
    fprintf(stdout, "\nВторой тест начался:\n\n");
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);
    if (!temp1 || !temp2) {fprintf(stderr, "Произошла ошибка\n\n");}
    debug_heap(stdout, heap);
    _free(temp1);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nВторой тест завершен\n\n");
    _free(temp2);
}


void release_two_blocks_test() {
    fprintf(stdout, "\nТретий тест начался\n\n");
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(1500);
    void* temp3 = _malloc(3000);
    if (!temp1 || !temp2 || !temp3) {fprintf(stderr, "Произошла ошибка\n\n");}
    debug_heap(stdout, heap);
    _free(temp1);
    debug_heap(stdout, heap);
    _free(temp2);
    debug_heap(stdout, heap);
    fprintf(stdout, "\nТретий тест завершен\n\n");
    _free(temp3);
}

void memory_expansion_test() {
    fprintf(stdout, "\nЧетвертый тест начался\n\n");
    void* temp1 = _malloc(1000);
    void* temp2 = _malloc(2000);
    void* temp3 = _malloc(10000);
    if (!temp1 || !temp2 || !temp3) {fprintf(stderr, "Произошла ошибка\n\n");}
    debug_heap(stdout, heap);
    fprintf(stdout, "\nЧетвертый тест завершен\n\n");
    _free(temp1);
    _free(temp2);
    _free(temp3);
}

void new_region_test() {
    fprintf(stdout, "\nПятый тест начался\n\n");
    void* temp1 = _malloc(8000);
    void* temp2 = _malloc(20000);
    void* temp3 = _malloc(100000);
    if (!temp1 || !temp2 || !temp3) {fprintf(stderr, "Произошла ошибка\n\n");}
    debug_heap(stdout, heap);
    fprintf(stdout, "\nПятый тест завершен\n\n");\
    _free(temp1);
    _free(temp2);
    _free(temp3);
}
