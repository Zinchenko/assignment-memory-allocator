#include "test.h"



int main() {
    heap_initialize(10000);
    mem_aloc_test();
    release_one_block_test();
    release_two_blocks_test();
    memory_expansion_test();
    new_region_test();
    fprintf(stdout, "Тесты окончены");
    return 0;
}

